package com.fxtcn.platform.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fxtcn.platform.dao.ProcessDefDao;
import com.fxtcn.platform.entity.ProcessDefEntity;
import com.fxtcn.platform.service.ProcessDefService;
import com.fxtcn.platform.util.Parametermap;
@Service
public class ProcessDefServiceImpl implements ProcessDefService {
	@Autowired
	private ProcessDefDao ProcessDefDao;
	@Override
	public List<ProcessDefEntity> queryPageAllProcessDef(Parametermap pm) {
		return ProcessDefDao.queryPageAllProcessDefPage(pm);
	}

}
