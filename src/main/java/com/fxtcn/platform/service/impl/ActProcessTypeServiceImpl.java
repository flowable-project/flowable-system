package com.fxtcn.platform.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fxtcn.platform.dao.ActProcessTypeMapper;
import com.fxtcn.platform.entity.ActProcessType;
import com.fxtcn.platform.service.ActProcessTypeService;
@Service("actProcessTypeServiceImpl")
public class ActProcessTypeServiceImpl implements ActProcessTypeService{

	@Autowired
	ActProcessTypeMapper actProcessTypeMapper;
	@Override
	public List<ActProcessType> selectAll() {
		return actProcessTypeMapper.selectAll();
	}

	@Override
	public int insert(ActProcessType record) {
		
		return actProcessTypeMapper.insert(record);
	}
}
