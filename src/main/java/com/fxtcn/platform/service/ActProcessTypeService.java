package com.fxtcn.platform.service;

import java.util.List;

import com.fxtcn.platform.entity.ActProcessType;

public interface ActProcessTypeService {
	   List<ActProcessType> selectAll();
	   public int insert(ActProcessType record);
}
