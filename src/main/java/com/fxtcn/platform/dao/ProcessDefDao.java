package com.fxtcn.platform.dao;

import java.util.List;

import com.fxtcn.platform.entity.ProcessDefEntity;
import com.fxtcn.platform.util.Parametermap;

/**
 */
public interface ProcessDefDao {
    /**
     * 查询所有已经部署的流程
     * @param pm
     * @return
     */
    public List<ProcessDefEntity> queryPageAllProcessDefPage(Parametermap pm);

}
